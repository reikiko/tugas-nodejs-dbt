const convertHexToRGBA = (hexCode, opacity = 1) => {
  let hex = hexCode.replace("#", "");

  if (hex.length === 3) {
    hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`;
  }

  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);

  /* Backward compatibility for whole number based opacity values. */
  if (opacity > 1 && opacity <= 100) {
    opacity = opacity / 100;
  }

  return `rgba(${r},${g},${b},${opacity})`;
};

module.exports = {
  convertHexToRGBA,
};

const converter = async (req, res) => {
  try {
    let body = "";
    req.on("data", (chunk) => {
      body += chunk.toString();
    });

    req.on("end", () => {
      const { color, opacity } = JSON.parse(body);
      const value = convertHexToRGBA(color, opacity);

      res.writeHead(200, { contentType: "application/json" });
      return res.end(JSON.stringify({ color: value }));
    });
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  converter,
};
