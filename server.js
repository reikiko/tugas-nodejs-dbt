const http = require("http");
const { converter } = require("./convert");

const port = process.env.PORT || 8080;

const server = http.createServer((req, res) => {
  if (req.url === "/api/0000ff" && req.method === "POST") {
    converter(req, res);
  } else {
    res.writeHead(404, { contentType: "application/json" });
    res.end(JSON.stringify({ message: "Route Not Found" }));
  }
});

server.listen(port, () => {
  console.log(`Server running : Port ${port}`);
});
